//domID
function domID(id) {
  return document.getElementById(id);
}

//Bài 1: Tìm số nguyên dương nhỏ nhất
domID("btn-tim-so-nguyen-duong-min").onclick = function () {
  //input
  var tong = 0;
  for (var n = 1; n < 1000; n++) {
    tong += n;
    if (tong >= 1000) {
      break;
    } else {
      continue;
    }
  }
  //   output
  domID(
    "xuat-ket-qua"
  ).innerHTML = `Kết quả: Số nguyên dương nhỏ nhất thoả điều kiện: "1 + 2 + ... +
  n > 1000" là: ${n}`;
};

//Bài 2: Tính Tổng theo hàm
domID("btn-tinh-tong").onclick = function () {
  // input
  var soX = domID("txt-so-x").value;
  var soN = domID("txt-so-n").value;
  //ouput
  var result = 0;
  for (var j = 1; j <= soN; j++) {
    result += soX ** j;
  }
  domID("xuat-ket-qua-tinh-tong").innerHTML = `Kết quả: Tổng = ${result}`;
};

//Bài 3: Tính Giai Thừa
domID("btn-tinh-giai-thua").onclick = function () {
  //input
  var number = domID("txt-number").value;
  //output
  var result = 1;
  //progress
  for (var i = 1; i <= number; i++) {
    result *= i;
  }
  domID(
    "xuat-ket-qua-tinh-giai-thua"
  ).innerHTML = `Kết quả: ${number}! = ${result.toLocaleString()}`;
};

//Bài 4: Tạo thẻ Div Chẵn và Lẻ

domID("btn-tao-the-div").onclick = function () {
  var theDiv = "";
  for (var i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
      theDiv += `<div class='alert alert-danger p-2'>Div chẵn</div>`;
    } else {
      theDiv += `<div class='alert alert-primary p-2'>Div lẻ </div>`;
    }
    domID("xuat-ket-qua-the-div").innerHTML = theDiv;
  }
};

//Bài 5:  In số nguyên tố
domID("btn-tim-so-nguyen-to").onclick = function () {
  //input
  var soN = domID("txt-so-N").value * 1;
  //output
  var result = "";
  //progress
  for (var a = 1; a <= soN; a++) {
    if (isPrime(a) == true) {
      result += " " + a;
    } else {
      continue;
    }
  }
  domID("xuat-ket-qua-so-nguyen-to").innerHTML = result;
};

//stackoverflow
function isPrime(num) {
  var sqrtnum = Math.floor(Math.sqrt(num));
  var prime = num != 1;
  for (var i = 2; i <= sqrtnum; i++) {
    if (num % i == 0) {
      prime = false;
      break;
    }
  }
  return prime;
}

// //Bài 5:  In số nguyên tố
// domID("btn-tim-so-nguyen-to").onclick = function () {
//   //input
//   var soN = domID("txt-so-N").value * 1;

//   //output
//   var result = "";

//   //progress
//   for (var n = 1; n <= soN; n++) {
//     console.log(Math.sqrt(n));
//     checkPrime(n) && (result += " " + n);
//   }
//   domID("xuat-ket-qua-so-nguyen-to").innerHTML = result;
// };

// function checkPrime(e) {
//   if (e < 2) return !1;
//   for (var t = 2; t <= Math.sqrt(e); t++) if (e % t == 0) return !1;
//   return !0;
// }
